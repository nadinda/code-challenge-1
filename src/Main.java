import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String nama, alamat;
        Double tinggiBadan, noSepatu;
        int umur;

        Scanner in = new Scanner(System.in);

        System.out.println("nama");
        nama = in.nextLine();
        System.out.println("nama saya: " + nama);

        System.out.println("alamat");
        alamat = in.nextLine();
        System.out.println("alamat saya: " + alamat);

        System.out.println("tinggi badan");
        tinggiBadan = in.nextDouble();
        System.out.println("tinggi badan saya: " + tinggiBadan);

        System.out.println("umur");
        umur = in.nextInt();
        System.out.println("umur saya: " + umur);

        System.out.println("no sepatu");
        noSepatu = in.nextDouble();
        System.out.println("no sepatu kaki saya: " + noSepatu);
    }
}
